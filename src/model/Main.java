package model;

import static org.junit.Assert.assertTrue;

import java.util.Vector;

public class Main
{

	public static void main(String[] args)
	{
		PokerHand myHand;
		Deck myDeck = new Deck();
		myHand= new PokerHand(5);
		
		boolean worked = true;
		Vector<Card> cardsDrawn = new Vector<Card>();
		Vector<Card> cardsAdded = new Vector<Card>();
		for (int i = 0; i < myDeck.getFullDeckSize(); i++)
		{
			cardsDrawn.add(myDeck.draw());
		}

		for (CardSuit s : CardSuit.values())
		{
			for (CardType c : CardType.values())
			{
				Card temp = new Card(s, c, null);
				cardsAdded.add(temp);
			}
		}
		cardsDrawn.containsAll(cardsAdded);
		System.out.println(cardsDrawn.equals(cardsAdded));

	}

}
